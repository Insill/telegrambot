# TelegramBot 

TelegramBot scrapes event data from various websites and puts it into a SQLite database. The event data is later fetched from the database and posted into a Telegram channel.
This bot's development process began in July 2020 and continues to this day.

Currently the plan is to include support for the Mobilizon platform through a GraphQL API. Posting events has to be also refactored in order to be separate for Telegram and Mobilizon. 
Additional fixes have been periodically made for scraping in case the websites used have made changes into their HTML formatting. 

Previously TelegramBot could scrape Facebook event pages with [Event-scrapper by berettavexee](https://github.com/berettavexee/events-scrapper). 
As Facebook made changes that made scraping harder, Event-scrapper coud no longer work and it has been since deleted from the project. 
A section _Facebook Pages_ still remains in _config.ini_ in case someone develops a working script for scraping Facebook.

 
## Structure

#### Scrape

Scraping event data is done separately and differently for each website. In general, BeautifulSoup is used for parsing HTML content and finding the elements, inside which are the attributes for each event.
In case of sites where the event data can't be parsed directly, Selenium is used first for getting the HTML content, after which BeautifulSoup is used. The scraped data for each site is a list that contains
a dictionary. 

After scraping the data, it gets inserted into the database. 

#### Db_create

Used for creating the bot's SQLite database. A database table for an event consists of the event's name, date, location, description, link for additional information and 
a timestamp. In order to prevent duplicate events being inserted, an UNIQUE constraint is applied on an event's name and date.

#### Database 

Contains all database procedures. The scraped data is inserted into a database with a timestamp. Later the bot fetches the data through this module and the timestamp of the latest event is added to
_config.ini_.

#### Bot

The main module for all bot functionality. When started, the settings are read from _config.ini_. _ConnectionChecker_ is used to check the Internet connection. _Scrape_ is called for scraping data from websites
and in case there are any new events, procedure _get_handle_post_ will be used for getting the event data, formatting it and eventually posting it. 
The bot's activities are scheduled using Python's _schedule_ library based on the variables _interval_days_ (how many days apart), _interval_minutes_ (how many minutes apart) and _hour_at_ 
(what time an activity starts). The events to be posted are fetched from the database and then formatted into Telegram posts. They are then posted into a channel by using the _python-telegram-bot_ library.

#### Common_log

Used for creating a common logger for each module. 

#### ConnectionChecker

Used by the bot to check and log the Internet connection's state periodically.

## Usage 

The following external libraries are required: 

- python-telegram-bot

- Selenium (GeckoDriver is used)

- Firefox (due to above)

- Beautiful Soup

_(As Mobilizon support is currently in development, the following is about the bot's use on Telegram)_

Create a Telegram bot with [the BotFather](https://riptutorial.com/telegram-bot/example/25075/create-a-bot-with-the-botfather) in case you haven't.

Run _db_create.py_ in order to create the SQLite database. 

Edit _config.ini_. Copy the bot's API token into the _API Tokens_ section. Add channel name and GeckoDriver path into their respective places in _Configuration_ section. Adjust the variables _interval_days_,
_interval_minutes_ and _hour_at_ to your liking. 
 
Finally start the bot by running _bot.py_. All info and debug stuff is logged into the files _bot.log_ and _geckodriver.log_.
