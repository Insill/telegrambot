# -*- coding: utf-8 -*-
'''
Created on 14.07.2020

@author: Insill
'''
import telegram
import configparser
import schedule
import time
import sys
import common_log
import connectionchecker
import scrape
from database import Database
from datetime import datetime
import db_create

logger = common_log.CommonLog("bot",20).get_logger()
config = configparser.ConfigParser(interpolation=None)
config.read("config.ini")
bot = telegram.Bot(token=config.get("API Tokens", "telegram"))


def post_message(message):
    bot.send_message(chat_id=config.get("Configuration", "channel_id"), disable_web_page_preview=True, text=message)


def handle_data(data):
    events = []
    for item in data:
        name = item[1]
        date = datetime.strptime(item[2], "%Y-%m-%d").strftime("%d.%m.%Y")
        location = item[3]
        description = item[4]
        link = item[5]       
        event = compose_event(name, date, location, description, link)
        events.append(event)
    return events


def compose_event(name, date, location, description, link):
    event = ""
    event += f"\U0001F3B5 {name} \U0001F3B5\n" # notes
    event += f"\U0001F4C5 {date}\n" # calendar
    event += f"\U0001F5FA {location}\n" # map
    event += f"{description}\n"
    event += f"\U0001F517 {link}" # link
    return event


def post_events(events):
    for event in events:
        post_message(event)
        time.sleep(10)

        
def get_handle_post(events):
    db = Database()
    data = db.get_data()
    db.close()
    if data:
        events = handle_data(data)
        logger.info("Posting events")
        post_events(events)
        logger.info("All events posted")
    else:
        logger.info("No new events")

         
def main():
    events = []
    db_create.main()
    logger.info("Bot started")
    interval_days = int(config.get("Configuration", "interval_days"))
    hour_at = config.get("Configuration", "hour_at")
    conn = connectionchecker.ConnectionChecker()

    schedule.every(interval_days).days.at(hour_at).do(conn.is_connected)
    schedule.every(interval_days).days.at(hour_at).do(scrape.main)
    schedule.every(interval_days).days.at(hour_at).do(get_handle_post, events)

    while True:
        try: 
            schedule.run_pending()
            time.sleep(1)
        except:
            logger.exception("Exception")
            sys.exit()

main()