import requests
import common_log

logger = common_log.CommonLog("connection",20).get_logger()

class ConnectionChecker:
    def __init__(self):
        pass

    def is_connected(self):
        url = "https://www.high.fi"
        try: 
            response = requests.head(url, timeout=1)
            if (response.status_code == 200):
                logger.info("Internet connection fine")
                return True
        except requests.ConnectionError as ex: 
            logger.warning(f"Internet connection down. {ex}")
            return False
