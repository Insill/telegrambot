#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on 17.12.2021

@author: Insill
'''
import sqlite3
import configparser
import os
import common_log
from datetime import datetime

class Database(object): 

    def __init__(self):
        this_folder = os.path.dirname(os.path.abspath(__file__))
        self.config_file = os.path.join(this_folder, "config.ini")
        self.logger = common_log.CommonLog("database",20).get_logger()
        self.config = configparser.ConfigParser(interpolation=None)
        self.config.read(self.config_file)
        self.connection = sqlite3.connect(os.path.join(this_folder,self.config.get("Configuration", "database")))


    def handle_data(self, data): 
        for i in range(len(data)):
            name = data[i]["name"]
            date = self.parse_date(data[i]["date"])
            location = data[i]["location"]
            try:
                description = data[i]["description"]
            except:
                description = ""
            link = data[i]["link"]
            self.insert(name, date, location, description, link)

        self.logger.info("{} rows changed.".format(self.connection.total_changes))


    def get_data(self):
        data = []
        added_latest = self.config.get("Configuration", "added_latest")
        cursor = self.connection.cursor()
        try:
            sql_get_events = '''SELECT * FROM events
            WHERE date >= date("now") AND added > ?
            ORDER BY date ASC;'''
            cursor.execute(sql_get_events, (added_latest,))
            data = cursor.fetchall()
        except:
            self.logger.exception("Exception")
    
        if data: 
            self.update_added_latest()
            return data


    def parse_date(self,date):  
        try: 
            date = datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m-%d")
        except ValueError:
            self.logger.exception("Exception")
        return date


    def insert(self, name, date, location, description, link):
        try: 
            cursor = self.connection.cursor()
            added = datetime.now().strftime("%Y-%m-%d")
            cursor.execute('''INSERT INTO events(name,date,location,description,link,added)
            VALUES(?, ?, ?, ?, ?, ?)''', (name, date, location, description, link, added))
            self.connection.commit()
        except sqlite3.IntegrityError:
            pass


    def update_added_latest(self):
        added_latest = self.config.get("Configuration", "added_latest")
        sql_get_added = '''SELECT DISTINCT added FROM events
        WHERE added > date(?) ORDER BY added DESC;'''
        cursor = self.connection.cursor()
        cursor.execute(sql_get_added, (added_latest,))
        date = cursor.fetchone()[0]
        if date > added_latest:
            added_latest = date
            self.config.set("Configuration", "added_latest", added_latest)
            self.configfile = open("config.ini", "w")
            self.config.write(self.configfile)
            self.configfile.close()


    def close(self):
        self.connection.close()