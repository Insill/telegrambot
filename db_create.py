#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on 13.07.2020

@author: Insill
'''
import sqlite3
import logging
import os.path
import common_log
logger = common_log.CommonLog("database",logging.INFO).get_logger()


def db_create() :
    sql_create_table = '''CREATE TABLE IF NOT EXISTS events
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            name        TEXT    NOT NULL,
            date        DATE   NOT NULL,
            location    TEXT    NOT NULL,
            description TEXT,
            link TEXT,
            added DATE, 
            UNIQUE(name, date));
        '''
    conn = sqlite3.connect("events.db")
    conn.execute(sql_create_table)
    logger.info("Database created!")

    conn.close()

    
def main():
    if not os.path.exists("events.db"):
        db_create()

main()
        