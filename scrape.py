#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on 12.07.2020

@author: Insill
'''
import urllib.request
import configparser
import json
from database import Database
import os
import common_log
import time
import psutil
from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
this_folder = os.path.dirname(os.path.abspath(__file__))
config_filepath = os.path.join(this_folder, "config.ini")
config = configparser.ConfigParser(interpolation=None)
config.read(config_filepath)
logger = common_log.CommonLog("scrape",10).get_logger()

def scrape_meteli():
    now = datetime.now()
    data = []
    url = config.get("URLs", "meteli")
    try:
        url_opened = urllib.request.urlopen(url) 
        webpage = BeautifulSoup(url_opened, "html.parser")
        script = str(webpage.find_all("script")[11])
        script_events = script[62:-9].strip()
        events = json.loads(script_events)
        for event in events:
            item = {}
            item.update({"name": event["name"]})
            date = datetime.strptime(event["date"], "%d.%m.").strftime(f"{now.strftime('%Y')}-%m-%d")
            item.update({"date": date})
            item.update({"location": event["location"]})
            item.update({"description": ""})
            item.update({"link": event["link"]})
            data.append(item)
    except: 
        logger.exception("Exception")
    finally:
        if data:
            logger.info("Scraped Meteli.net")
            return data
        else:
            return False


def menoinfo_get_url():
    url = config.get("URLs","menoinfo")
    start = "="
    end = "&"
    curr_date = datetime.now().strftime("%d.%m.%Y")
    url_date = url[url.find(start) + 1 : url.find(end)]
    new_url = url.replace(url_date,curr_date)
    return new_url

    
def scrape_menoinfo():
    data = []
    listevents = []
    try: 
        gdriver_path = config.get("Configuration", "geckodriver_path")
        options = Options()
        options.headless = True
        ser = Service(gdriver_path)
        time.sleep(10)
        browser = webdriver.Firefox(service=ser,options=options)
        browser.set_page_load_timeout(60)
        url = menoinfo_get_url()
        browser.get(url)
        webpage = BeautifulSoup(browser.page_source, "html.parser")
        listevents = webpage.find_all(attrs={"class": ["e2e-box e2e-listevent e2e-haspic","e2e-box e2e-listevent"]})
        webpage.find_all(recursive=True)
        browser.quit()
        for proc in psutil.process_iter():
            if proc.name() == "geckodriver":
                proc.kill()
    except: 
        logger.exception("Exception")
    finally:   
        if listevents:
            for event in listevents:
                item = {}
                name = event.find("h2").text
                item.update({"name": name})
                temp = event.find_all("p")[0].text
                date_temp = temp.split("|")[0].replace("-", "").strip()
                date = date_temp.split()[0]
                item.update({"date": datetime.strptime(date, "%d.%m.%Y").strftime("%Y-%m-%d")})
                location = temp.split("|")[2]
                item.update({"location":location})
                description = event.find(class_="e2e-textcontent").text
                item.update({"description":description})
                event_id = event.find_all("a")[0].attrs["href"][1:15]
                link = "https://savonsanomat.menoinfo.fi/#" + event_id
                item.update({"link": link})
                data.append(item)

            logger.info("Scraped MenoInfo")
            return data
        else: 
            return False


def main():
    logger.info("Scraping started")
    db = Database()
    data1 = scrape_meteli()
    if data1:
        db.handle_data(data1)
    data2 = scrape_menoinfo()
    if data2: 
        db.handle_data(data2)
    logger.info("Scraping complete")
    db.close()